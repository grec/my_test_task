import json
#import logging
import logging.config
import re
import socket


import settings as settings


logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger(__name__)
logger.info('Start application...')


def check_account(data):
    if not re.match(r'^\d+$', data):
        return False, None
    return True, data


def check_password(data):
    if not re.match(r'^[A-z0-9]+$', data):
        return False, None
    return True, data


def check_amount(data):
    if not re.match(r'^\d+(\.\d{,2})?$', data):
        return False, None
    return True, data


def get_login_data():
    print('Enter your account')
    account = check_account(input())
    print('Enter your password')
    password = check_password(input())
    return account[0] & password[0], account[1], password[1]


def login():
    return


def quit(args, ):
    exit(0)


def get_balance(args, ):
    conn = args[0]
    conn.send(str.encode(json.dumps({"action": "balance", })))
    data = conn.recv(1024)
    ans = json.loads(data.decode())
    if "result" in ans and "ok" == ans["result"]:
        print("Your balance: %s y.e" % ans["balance"])


def help(args, ):
    print('avaliable commands:')
    print('balance - get your balance')
    print('payment - make payment')
    print('quit - exit from program')


def unknown_command(args,):
    print('Unknown command!')


def payment(args,):
    conn = args[0]
    print('Enter receipent account')
    account = check_account(input())
    print('Enter amount')
    amount = check_amount(input())
    if not account[0] or not amount[0]:
        print('Invalid account number or amount')
        return
    conn.send(str.encode(json.dumps({"action": "payment", "receipient": account[1], "amount": amount[1]})))
    data = conn.recv(1024)
    ans = json.loads(data.decode())
    if "result" in ans and "ok" == ans["result"]:
        print("Your balance: %s y.e" % ans["balance"])
    if "result" in ans and "error" == ans["result"]:
        print("%s" % ans["message"])


commands = {"balance": get_balance, "help": help, "quit": quit, "default": unknown_command, "payment": payment}


def main():

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((settings.SERVER_HOST, settings.SERVER_PORT))
        checked, account, password = get_login_data()
        conn.send(str.encode(json.dumps({"action": "login", "account": account, "password": password})))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        if "result" in ans and "ok" == ans["result"]:
            print("Your balance: %s y.e" % ans["balance"])
        else:
            print("%s" % ans["message"])
            exit(0)
        while True:
            print("Type 'help' to get tips")
            command = input()
            if command not in commands:
                f = commands["default"]
            else:
                f = commands[command]
            f((conn,))


if __name__ == '__main__':
    main()
