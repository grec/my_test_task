import unittest

from client.main import check_account, check_password, check_amount


class TestCheckAccount(unittest.TestCase):

    def test_positive_only_digits(self):
        self.assertTupleEqual(check_account("0"), (True, "0"))

    def test_negative_only_letters(self):
        self.assertTupleEqual(check_account("abc"), (False, None))

    def test_negative_mixed(self):
        self.assertTupleEqual(check_account("123abc"), (False, None))

    def test_negative_spec_symbols(self):
        self.assertTupleEqual(check_account("12.0"), (False, None))


class TestCheckPassword(unittest.TestCase):

    def test_positive_only_digits(self):
        self.assertTupleEqual(check_password("0"), (True, "0"))

    def test_positive_only_letters(self):
        self.assertTupleEqual(check_password("abc"), (True, "abc"))

    def test_positive_mixed(self):
        self.assertTupleEqual(check_password("123abc"), (True, "123abc"))

    def test_negative_special_symbols(self):
        self.assertTupleEqual(check_password("123abc."), (False, None))


class TestCheckAmount(unittest.TestCase):

    def test_positive_integer_1(self):
        self.assertTupleEqual(check_amount("0"), (True, "0"))

    def test_positive_integer_2(self):
        self.assertTupleEqual(check_amount("100"), (True, "100"))

    def test_positive_float(self):
        self.assertTupleEqual(check_amount("100.01"), (True, "100.01"))

    def test_negative_float_with_more_precision(self):
        self.assertTupleEqual(check_amount("100.0001"), (False, None))


if __name__ == "__main__":
    unittest.main()
