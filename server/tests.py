import json
import socket
import sys


def test_negative_bad_command(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"account": "1", "password": "1"})))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "error", "message": "Wrong command format"},\
            "Server answer %s" % str(ans)


def test_negative_without_arguments(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "1", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "error", "message": "Illegal command arguments"},\
            "Server answer %s" % str(ans)



def test_negative_bad_arguments(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "1abc", "password": "123"})))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "error", "message": "Illegal command arguments"},\
            "Server answer %s" % str(ans)



def test_positive_login(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "1", "password": "123"})))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "ok", "balance": "100.00"},\
            "Server answer %s" % str(ans)


def test_negative_login(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "1", "password": "123abc"})))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "error", "message": "Account, password pair not founded"},\
            "Server answer %s" % str(ans)


def test_positive_balance(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "1", "password": "123"})))
        data = conn.recv(1024)

        conn.send(str.encode(json.dumps({"action": "balance", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "ok", "balance": "100.00"},\
            "Server answer %s" % str(ans)


def test_negative_balance(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "balance", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "error", "message": "Operation denied"},\
            "Server answer %s" % str(ans)


def test_positive_balance_extra_parameters(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "1", "password": "123"})))
        data = conn.recv(1024)

        conn.send(str.encode(json.dumps({"action": "balance", "account":"2", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "ok", "balance": "100.00"},\
            "Server answer %s" % str(ans)


def test_negative_balance_extra_parameters(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "balance", "account": "1", "password": "123"})))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "error", "message": "Operation denied"},\
            "Server answer %s" % str(ans)


def test_positive_payment(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "2", "password": "123"})))
        data = conn.recv(1024)

        conn.send(str.encode(json.dumps({"action": "payment", "receipient": "1", "amount": "10.00", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "ok", "balance": "990.00"},\
            "Server answer %s" % str(ans)


def test_positive_payment_float(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "2", "password": "123"})))
        data = conn.recv(1024)

        conn.send(str.encode(json.dumps({"action": "payment", "receipient": "1", "amount": "5.55", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "ok", "balance": "984.45"},\
            "Server answer %s" % str(ans)


def test_negative_payment_themself(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "1", "password": "123"})))
        data = conn.recv(1024)

        conn.send(str.encode(json.dumps({"action": "payment", "receipient": "1", "amount": "10.00", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "error", "message": "Illegal receipient account"},\
            "Server answer %s" % str(ans)


def test_negative_payment_wallet_overflow(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "1", "password": "123"})))
        data = conn.recv(1024)

        conn.send(str.encode(json.dumps({"action": "payment", "receipient": "3", "amount": "10.00", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "error", "message": "Receipient wallet is overflow"},\
            "Server answer %s" % str(ans)


def test_negative_payment_big_amount(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "2", "password": "123"})))
        data = conn.recv(1024)

        conn.send(str.encode(json.dumps({"action": "payment", "receipient": "1", "amount": "100000.00", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "error", "message": "Insufficient funds"},\
            "Server answer %s" % str(ans)


def test_positive_payment_50(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "4", "password": "123"})))
        data = conn.recv(1024)

        conn.send(str.encode(json.dumps({"action": "payment", "receipient": "1", "amount": "50.00", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "ok", "balance": "0.00"},\
            "Server answer %s" % str(ans)


def test_positive_payment_little_more(server, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((server, port))
        conn.send(str.encode(json.dumps({"action": "login", "account": "5", "password": "123"})))
        data = conn.recv(1024)

        conn.send(str.encode(json.dumps({"action": "payment", "receipient": "1", "amount": "50.00000001", })))
        data = conn.recv(1024)
        ans = json.loads(data.decode())
        assert ans == {"result": "error", "message": "Illegal command arguments"},\
            "Server answer %s" % str(ans)






tests = [
    test_negative_bad_command,
    test_negative_without_arguments,
    test_negative_bad_arguments,
    test_positive_login,
    test_negative_login,
    test_positive_balance,
    test_negative_balance,
    test_positive_balance_extra_parameters,
    test_negative_balance_extra_parameters,
    test_positive_payment,
    test_positive_payment_float,
    test_negative_payment_themself,
    test_negative_payment_wallet_overflow,
    test_negative_payment_big_amount,
    test_positive_payment_50,
    test_positive_payment_little_more,

]


def main():
    server = sys.argv[1]
    port = int(sys.argv[2])
    print("server: %s" % server)
    print("port: %d" % port)
    for test in tests:
        try:
            test(server, port)
            print("Test %-40s\tOk" % (test.__name__, ))
        except AssertionError as e:
            print("Test %s failed! %s" % (test.__name__, e))


if "__main__" == __name__:
    main()

