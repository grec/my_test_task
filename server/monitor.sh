#!/bin/bash


PORT=300



while True 
do
	clear;

	echo 
	echo 
	echo 
	echo 
	echo 'Running processes:'
	ps -el | grep main\.py | grep -v grep

	echo 
	echo 
	echo 
	echo 
	echo 'Network activity:'	
	netstat -na | grep \.$PORT | grep -E 'LISTEN|ESTABLISHED'
	sleep 2;

done;

	
