import logging
import re
import sqlite3
from abc import ABC, abstractmethod

from settings import BALANCE_LIMIT


class Wallet():
    def __init__(self, account, password, balance):
        self._account = account
        self._password = password
        self._balance = balance

    def is_authorized(self):
        if self._account:
            return True
        else:
            return False

    def set_balance(self, balance):
        self._balance = balance

    def get_balance(self):
        return "%.2f" % self._balance

    def get_account(self):
        #return "%d" % \
        return self._account


def check_account(data):
    if not re.match(r'^\d+$', data):
        return False, None
    return True, data


def check_password(data):
    if not re.match(r'^[A-z0-9]+$', data):
        return False, None
    return True, data


def check_amount(data):
    if not re.match(r'^\d+(\.\d{,2})?$', data):
        return False, None
    return True, data


class Worker(ABC):

    @abstractmethod
    def authorize(self):
        pass

    @abstractmethod
    def authentificate(self, args):
        return self.authentificate(args)

    @abstractmethod
    def get_balance(self, args):
        return self.get_balance(args)

    @abstractmethod
    def payment(self, args):
        return self.payment(args)

    @abstractmethod
    def handle_command(self, command):
        return self.handle_command(self, command)


class WrongFormatException(BaseException):
    pass


class WrongCommandException(BaseException):
    pass


class OperationDenied(BaseException):
    pass


class InsufficientFundsException(BaseException):
    pass


class LimitExceededException(BaseException):
    pass


class ReceipentAccountException(BaseException):
    pass


class ReceipentNotFoundedException(BaseException):
    pass

class IllegalCommandArgument(BaseException):
    pass


class SQLiteWorker(Worker):
    _wallet = None
    _commands = {"login": Worker.authentificate, "balance": Worker.get_balance, "payment": Worker.payment, }
    logger = logging.getLogger(__name__)

    def authorize(self):
        pass

    def authentificate(self, args):
        try:
            valid, account = check_account(args['account'])
            if not valid:
                raise IllegalCommandArgument
            valid, password = check_password(args['password'])
            if not valid:
                raise IllegalCommandArgument
        except:
            raise IllegalCommandArgument
        conn = sqlite3.connect('test.db')
        c = conn.cursor()
        wallet = c.execute("SELECT * FROM wallets where account = :account and password = :password",
                           {"account": account, "password": password}).fetchone()
        conn.close()
        if wallet:
            self._wallet = Wallet(int(wallet[0]), wallet[1], float(wallet[2]))
            return {"result": "ok", "balance": self._wallet.get_balance()}
        else:
            return {"result": "error", "message": "Account, password pair not founded"}

    def get_balance(self, args):
        if self._wallet and self._wallet.is_authorized:
            conn = sqlite3.connect('test.db')
            c = conn.cursor()
            wallet = c.execute("SELECT * FROM wallets where account = :account",
                               {"account": self._wallet.get_account()}).fetchone()
            conn.close()

            self._wallet.set_balance(float(wallet[2]))
            return {"result": "ok", "balance": self._wallet.get_balance()}
        else:
            raise OperationDenied

    def payment(self, args):
        try:
            valid, acc = check_account(args["receipient"])
            if not valid:
                raise IllegalCommandArgument
            receipient = int(acc)
            valid, am = check_amount(args["amount"])
            if not valid:
                raise IllegalCommandArgument
            amount = float(am)
        except:
            raise IllegalCommandArgument
        if self._wallet and self._wallet.is_authorized:

            self.logger.debug("sender account: %d, receipient account: %d" % (self._wallet.get_account(), receipient))
            if self._wallet.get_account() == receipient:
                raise ReceipentAccountException
            with sqlite3.connect('test.db') as conn:
                c = conn.cursor()
                wallet = c.execute("SELECT * FROM wallets where account = :account",
                                   {"account": self._wallet.get_account()}).fetchone()
                sender_wallet = Wallet(int(wallet[0]), wallet[1], float(wallet[2]))
                sender_wallet._balance -= amount
                if sender_wallet._balance < 0.0:
                    raise InsufficientFundsException
                wallet = c.execute("SELECT * FROM wallets where account = :account",
                                       {"account": receipient}).fetchone()
                receipient_wallet = Wallet(int(wallet[0]), wallet[1], float(wallet[2]))
                if not receipient_wallet:
                    raise ReceipentNotFoundedException
                receipient_wallet._balance += amount
                if receipient_wallet._balance >= BALANCE_LIMIT:
                    raise LimitExceededException
                c.execute("UPDATE wallets SET balance = :balance where account = :account",
                          {"account": sender_wallet._account, "balance": sender_wallet._balance}).fetchone()
                c.execute("UPDATE wallets SET balance = :balance where account = :account",
                          {"account": receipient_wallet._account, "balance": receipient_wallet._balance}).fetchone()
                conn.commit()
                self._wallet = sender_wallet
                return {"result": "ok", "balance": self._wallet.get_balance()}
        else:
            raise OperationDenied

    def handle_command(self, command):
        if 'action' not in command:
            raise WrongFormatException

        if command['action'] not in self._commands:
            raise WrongCommandException

        f = self._commands[command['action']]
        return f(self, command)
