SERVER_HOST = '0.0.0.0'
SERVER_PORT = 300

LOGGING = {
    "version": 1,
    "handlers": {
        "fileHandler": {
            "class": "logging.FileHandler",
            "formatter": "myFormatter",
            "filename": "logs/debug.log"
        }
    },
    "loggers": {
        "__main__": {
            "handlers": ["fileHandler"],
            "level": "DEBUG",
        },
        "SQLiteWorker": {
            "handlers": ["fileHandler"],
            "level": "INFO",
        },
        "default": {
            "handlers": ["fileHandler"],
            "level": "INFO",
        }
    },
    "formatters": {
        "myFormatter": {
            "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        }
    }
}

BALANCE_LIMIT = 800.00