import json
import logging
import logging.config
import socket
import sqlite3

import settings as settings
from threading import Thread

from protocol import SQLiteWorker, WrongFormatException, WrongCommandException, OperationDenied, \
    InsufficientFundsException, LimitExceededException, ReceipentAccountException, ReceipentNotFoundedException, \
    IllegalCommandArgument

logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger(__name__)
logger.info('Start application...')


def serve_client(conn, addr):
    try:
        logger.info('New connection from %s' % str(addr))
        worker = SQLiteWorker()
        while True:
            #try:
                data = conn.recv(1024)
                if not data:
                    return
                request = data.decode()
                command = json.loads(request)
                logger.debug(command)
                try:
                    result = worker.handle_command(command)
                    answer = str.encode(json.dumps(result))
                except WrongFormatException as e:
                    answer = str.encode(json.dumps({"result": "error", "message": "Wrong command format"}))
                except WrongCommandException as e:
                    answer = str.encode(json.dumps({"result": "error", "message": "Wrong command"}))
                except OperationDenied as e:
                    answer = str.encode(json.dumps({"result": "error", "message": "Operation denied"}))
                except InsufficientFundsException as e:
                    answer = str.encode(json.dumps({"result": "error", "message": "Insufficient funds"}))
                except LimitExceededException as e:
                    answer = str.encode(json.dumps({"result": "error", "message": "Receipient wallet is overflow"}))
                except ReceipentAccountException as e:
                    answer = str.encode(json.dumps({"result": "error", "message": "Illegal receipient account"}))
                except ReceipentNotFoundedException as e:
                    answer = str.encode(json.dumps({"result": "error", "message": "Receipient account not founded"}))
                except IllegalCommandArgument as e:
                    answer = str.encode(json.dumps({"result": "error", "message": "Illegal command arguments"}))
                logger.debug(answer)
                conn.send(answer)
            # except Exception as e:
            #     print(e)
    except KeyboardInterrupt:
        conn.close()


def create_server():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((settings.SERVER_HOST, settings.SERVER_PORT))
        s.listen(1)
        while True:
            conn, addr = s.accept()
            t = Thread(target=serve_client, args=(conn, addr, ))
            t.start()


if __name__ == '__main__':
    create_server()
