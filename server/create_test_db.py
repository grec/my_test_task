import sqlite3


conn = sqlite3.connect('test.db')
c = conn.cursor()
c.execute("CREATE TABLE wallets (account  INTEGER PRIMARY KEY, password  TEXT, balance DOUBLE )")
c.execute("INSERT INTO wallets (password, balance) VALUES ('123',100.0)")
c.execute("INSERT INTO wallets (password, balance) VALUES ('123',1000.0)")
c.execute("INSERT INTO wallets (password, balance) VALUES ('123',2000.0)")
c.execute("INSERT INTO wallets (password, balance) VALUES ('123',50.0)")
c.execute("INSERT INTO wallets (password, balance) VALUES ('123',50.0)")
conn.commit()
conn.close()
