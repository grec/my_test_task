#!/bin/bash

#cd ../

#../venv/bin/activate

# drop logs and database
rm -rf logs/*
rm -rf test.db

# generate new database



../venv/bin/python create_test_db.py
../venv/bin/python main.py &
for i in 3 2 1 go!
do
    clear;
    echo $i;
    sleep 1;
done;
../venv/bin/python tests.py 127.0.0.1 300


# kill server process

ps aux | grep '[pP]ython main\.py' | awk '{print $2}' | xargs kill -9 $1